import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
            '한사랑 산악회♥',
          style: TextStyle(
            letterSpacing: -1.5,
            fontSize: 25,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.green,
        foregroundColor: Colors.white,
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/han_sa_rang.jpeg'),
              ),
              accountName: Text(
                '한사랑 산악회',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              accountEmail: Text(
                '열정! 열정! 열정!',
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w100,
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.green,
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: FractionalOffset.centerLeft,
              margin: EdgeInsets.fromLTRB(20, 20, 0, 20),
              child: Column(
                children: [
                  Text(
                      '한사랑 산악회를 소개합니다 !',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 25,
                      color: Colors.black87,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Image.asset(
                      'assets/han_sa_rang.jpeg',
                    width:410,
                    height: 230,
                    fit: BoxFit.cover,
                  )
                ],
              ),
            ),
            Container(
              alignment: FractionalOffset.centerLeft,
              margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      '한사랑 산악회',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 20,
                      color: Colors.black87,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Text(
                      'Feat.피식대학',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 16,
                      color: Colors.black54,
                      fontWeight: FontWeight.w100,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: FractionalOffset.centerLeft,
              margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '유튜브 보러가기',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 20,
                      color: Colors.black87,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Text(
                    'https://youtu.be/Hfzk05R_cqw?si=30reJetfwQheVGra',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 16,
                      color: Colors.black54,
                      fontWeight: FontWeight.w100,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 50, 0, 20),
              height:1.0,
              width:410.0,
              color:Colors.green,
            ),
            Container(
              alignment: FractionalOffset.centerLeft,
              margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '주요 멤버',
                    style: TextStyle(
                      letterSpacing: -1.5,
                      fontSize: 25,
                      color: Colors.black87,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: FractionalOffset.centerLeft,
              margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        'assets/kms.png',
                        width: 170,
                        height: 170,
                        fit: BoxFit.cover,
                      ),
                      Text(
                        '회장',
                        style: TextStyle(
                          letterSpacing: -1.5,
                          fontSize: 20,
                          color: Colors.black87,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        '김영남',
                        style: TextStyle(
                          letterSpacing: -1.5,
                          fontSize: 16,
                          color: Colors.black54,
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.fromLTRB(20, 20, 0, 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          'assets/lch.png',
                          width: 170,
                          height: 170,
                          fit: BoxFit.cover,
                        ),
                        Text(
                          '부회장',
                          style: TextStyle(
                            letterSpacing: -1.5,
                            fontSize: 20,
                            color: Colors.black87,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Text(
                          '이택조',
                          style: TextStyle(
                            letterSpacing: -1.5,
                            fontSize: 16,
                            color: Colors.black54,
                            fontWeight: FontWeight.w100,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              ),
            Container(
              alignment: FractionalOffset.centerLeft,
              margin: EdgeInsets.fromLTRB(20, 0, 0, 20),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                          'assets/jjh.png',
                        width: 170,
                        height: 170,
                        fit: BoxFit.cover,
                      ),
                      Text(
                          '총무',
                        style: TextStyle(
                          letterSpacing: -1.5,
                          fontSize: 20,
                          color: Colors.black87,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        '정광용',
                        style: TextStyle(
                          letterSpacing: -1.5,
                          fontSize: 16,
                          color: Colors.black54,
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.fromLTRB(20, 20, 0, 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          'assets/lyj.png',
                          width: 170,
                          height: 170,
                          fit: BoxFit.cover,
                        ),
                        Text(
                          '회원',
                          style: TextStyle(
                            letterSpacing: -1.5,
                            fontSize: 20,
                            color: Colors.black87,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Text(
                          '배용길',
                          style: TextStyle(
                            letterSpacing: -1.5,
                            fontSize: 16,
                            color: Colors.black54,
                            fontWeight: FontWeight.w100,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
